@echo off

if "%1" == "" goto :INFO
if /i "%1" == "build" goto :BUILD
if /i "%1" == "up" goto :UP
if /i "%1" == "start" goto :START
if /i "%1" == "stop" goto :STOP
if /i "%1" == "cleanup" goto :CLEANUP


:INFO
echo "dockerrun build" for building the image
echo "dockerrun up" for starting apache2 and mysql
echo "dockerrun start" for starting apache2 and mysql in background
echo "dockerrun stop" for stopping container
echo "dockerrun cleanup" for cleanup docker images
goto END

:BUILD
docker build . -t ulsmith/alpine-apache-php7
goto END

:UP
docker-compose up
goto END

:START
docker-compose up -d
goto END

:STOP
docker-compose stop
goto END

:CLEANUP
rem ## remove container
FOR /f "tokens=*" %%i IN ('docker ps -aq') DO docker rm %%i
rem ## remove images
FOR /f "tokens=*" %%i IN ('docker images --format "{{.ID}}"') DO docker rmi %%i
goto END

:END
