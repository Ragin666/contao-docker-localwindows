FROM ulsmith/alpine-apache-php7

ADD contao.conf /etc/apache2/conf.d/
RUN apk add php7-intl
